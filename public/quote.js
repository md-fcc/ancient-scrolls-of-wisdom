$(document).ready(function () {

    // -------------------------------------------------------------------------
    // ---------------------- Helper Functions ---------------------------------
    // -------------------------------------------------------------------------
    
    function shuffleArray(array) {
        // iterates backwards through array, swapping element index i with
        // a random element j located at an index between 0 and i.
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }
    
    // -------------------------------------------------------------------------
    // ----------------------------- MAIN --------------------------------------
    // -------------------------------------------------------------------------
    
    let quotes = [];
    let quote_index = 0;

    function showQuote() {
        if (quote_index >= quotes.length) {
            shuffleArray(quotes);
            quote_index = 0;
        }
        $("#quote").html(quotes[quote_index].quote);
        $("#author").html('- ' + quotes[quote_index].author);
        $(".tweet-button")
            .attr('href', encodeURI("https://twitter.com/intent/tweet?hashtags=quotes&text=" + 
                quotes[quote_index].quote + 
                " -" + quotes[quote_index].author));
                
        quote_index++;
    }

    fetch("data/quote_list.json")
        .then(response => response.json())
        .then(quotes_json => {
            quotes = shuffleArray(quotes_json);
            showQuote();
            $("[class*='hidden']").removeClass("hidden");
        });

    $("#next-quote").on('click', showQuote);
})