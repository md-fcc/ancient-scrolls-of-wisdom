My random quote website to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/build-a-random-quote-machine).

Live website: [Ancient Scrolls of Wisdom](https://md-fcc.gitlab.io/ancient-scrolls-of-wisdom/)

The Javascript code is relatively simple. I gathered 101 quotes that I found from various sources and stored them in a JSON file. The quotes are loaded and put in random order. Everytime the user clicks the button, the next quote is displayed. The Twitter bird icon can be clicked to tweet the quote.

